# OpenML dataset: GameStop-Historical-Stock-Prices

https://www.openml.org/d/43424

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

GameStop Corp. is an American video game, consumer electronics, and gaming merchandise retailer. GameStop's stocks have spiked in recent days since the Reddit group blew up the stock price. Now GameStop is up more than 1,700 since the start of January. so it is interesting to try to predict the stock prices in the next months and see how prediction and time series models will perform in this case. This  Dataset is a daily historical stock price of GameStop from February 2002 (the year it was listed) to January 2021. 

Content
The dataset contains:

Date: The date of trading
Open_price: The opening price of the stock
High_price: The high price of that day 
Low_price: The low price of that day
Close_price: The closed price of that day 
Volume: The amount of stocks traded during that day
Adjclose_price: The stock's closing price has been amended to include any distributions/corporate actions that occur before the next days open.

Acknowledgements
This Dataset collected from Yahoo finance website

Inspiration
How a sudden increasi in the volume affect stock trading? 
What the impact of the variance between the adjusted close and the next day's opening price?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43424) of an [OpenML dataset](https://www.openml.org/d/43424). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43424/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43424/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43424/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

